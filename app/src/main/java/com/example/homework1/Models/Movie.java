package com.example.homework1.Models;

import java.util.Comparator;
public class Movie{

    private String title, genre;

    private int imgId;

    public Movie() {
    }

    public Movie(String title, String genre, int imgId) {
        this.title = title;
        this.genre = genre;
        this.imgId = imgId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }


    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    public static Comparator<Movie> MovieTitleComparatorAscending = new Comparator<Movie>() {

        public int compare(Movie m1, Movie m2) {
            String MovieTitle1 = m1.getTitle().toUpperCase();
            String MovieTitle2 = m2.getTitle().toUpperCase();

            //ascending order
            return MovieTitle1.compareTo(MovieTitle2);
        }};

    public static Comparator<Movie> MovieTitleComparatorDescending = new Comparator<Movie>() {

        public int compare(Movie m1, Movie m2) {
            String MovieTitle1 = m1.getTitle().toUpperCase();
            String MovieTitle2 = m2.getTitle().toUpperCase();

            //descending order
            return MovieTitle2.compareTo(MovieTitle1);
        }};

}
