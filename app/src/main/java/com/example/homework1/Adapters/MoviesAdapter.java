package com.example.homework1.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.homework1.Models.Movie;
import com.example.homework1.R;

import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView titleTV, genreTV;
        public ImageView starIV;
        public ConstraintLayout constraintLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            starIV = (ImageView) itemView.findViewById(R.id.star_imageView_movie_row_id);
            titleTV = (TextView) itemView.findViewById(R.id.title_movie_row_id);
            genreTV = (TextView) itemView.findViewById(R.id.genre_movie_row_id);
            constraintLayout = (ConstraintLayout) itemView.findViewById(R.id.constraintLayout_movie_row_id);
        }
    }

    private List<Movie> movies;

    public MoviesAdapter(List<Movie> movies) {
        this.movies = movies;
    }

    @NonNull
    @Override
    public MoviesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesAdapter.MyViewHolder holder, int position) {
        Movie movie = movies.get(position);

        holder.starIV.setImageResource(movie.getImgId());
        holder.titleTV.setText(movie.getTitle());
        holder.genreTV.setText(movie.getGenre());
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }


}
