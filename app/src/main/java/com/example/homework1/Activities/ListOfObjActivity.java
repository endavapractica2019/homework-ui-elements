package com.example.homework1.Activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.homework1.Adapters.MoviesAdapter;
import com.example.homework1.Models.Movie;
import com.example.homework1.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ListOfObjActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private MoviesAdapter moviesAdapter;
    private List<Movie> movieList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_obj);

        recyclerView = findViewById(R.id.recycler_view_id);

        moviesAdapter = new MoviesAdapter(movieList);
        RecyclerView.LayoutManager movieLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(movieLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(moviesAdapter);

        prepareMovieData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.filter_a_z:
                Collections.sort(movieList, Movie.MovieTitleComparatorAscending);
                moviesAdapter.notifyDataSetChanged();
                return (true);
            case R.id.filter_z_a:
                Collections.sort(movieList, Movie.MovieTitleComparatorDescending);
                moviesAdapter.notifyDataSetChanged();
                return (true);
        }
        return (super.onOptionsItemSelected(item));
    }

    private void prepareMovieData() {
        Movie movie = new Movie("Mad Max: Fury Road", "Action & Adventure", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("Inside Out", "Animation, Kids & Family", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("Star Wars: Episode VII - The Force Awakens", "Action", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("Shaun the Sheep", "Animation", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("The Martian", "Science Fiction & Fantasy", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("Mission: Impossible Rogue Nation", "Action", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("Up", "Animation", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("Star Trek", "Science Fiction", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("The LEGO Movie", "Animation", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("Iron Man", "Action & Adventure", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("Aliens", "Science Fiction", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("Chicken Run", "Animation", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("Back to the Future", "Science Fiction", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("Raiders of the Lost Ark", "Action & Adventure", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("Goldfinger", "Action & Adventure", R.mipmap.ic_blue_star);
        movieList.add(movie);

        movie = new Movie("Guardians of the Galaxy", "Science Fiction & Fantasy", R.mipmap.ic_blue_star);
        movieList.add(movie);

        moviesAdapter.notifyDataSetChanged();
    }
}
