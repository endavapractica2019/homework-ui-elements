package com.example.homework1.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.example.homework1.R;

public class MainActivity extends AppCompatActivity {

    private static int noTheme = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (noTheme == 1) {
            setTheme(R.style.DarkTheme);

        } else if (noTheme == 2) {

            MainActivity.this.setTheme(R.style.BlueTheme);
        }
        setContentView(R.layout.activity_main);

        Spinner spinner = findViewById(R.id.select_theme_spinner_id);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 2 || i == 1) {
                    noTheme = i;
                    finish();
                    startActivity(new Intent(MainActivity.this, MainActivity.class));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Button button = (Button) findViewById(R.id.list_movies_button_id);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListOfObjActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();


    }
}
